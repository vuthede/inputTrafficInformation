package net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UdpSender {

	private boolean _opened = false;
	private DatagramSocket _clientSocket;
	private String _hostname = "";
	private InetAddress _IPAddress ;
	private int _port = 0;
        
        
	public void open(String hostname, int port) {
		// TODO given host name and port number of receiver,
		//		open a UDP socket using those arguments
		try {
			_clientSocket = new DatagramSocket();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			_IPAddress = InetAddress.getByName(hostname);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    _hostname = hostname;
	    _port = port;
	    _opened = true;
	}

	public void send(String data) {
		// TODO send data through UDP socket
		byte[] sendData = new byte[1024];
		sendData = data.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, _IPAddress, _port);
		try {
			_clientSocket.send(sendPacket);
			System.out.println("Send: " + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean opened() {
		return _opened;
	}

}

package net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UdpReceiver {
	private ConcurrentLinkedQueue<String> _queue = new ConcurrentLinkedQueue<String>();
	private Thread _taskListen;

	public void listen(int port, int size) {
		_taskListen = new Thread(new Runnable() {
			@Override
			public void run() {
				DatagramSocket serverSocket = null;
				byte[] receiveData = new byte[1024];

				try {
					serverSocket = new DatagramSocket(port);
					System.out.println("Open port: " + port);
				} catch (SocketException e1) {
					e1.printStackTrace();
				}

				while (true) {
					DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

					try {
						serverSocket.receive(receivePacket);
					} catch (IOException e) {
						e.printStackTrace();
					}

					String signal = new String(receivePacket.getData(), 0, receivePacket.getLength());
					
					System.out.println("Udp Receiver: " + signal);
					
					while (_queue.size() > size)
						_queue.poll();
					
					_queue.add(signal);
				}
			}
		});
		_taskListen.start();
	}

	public String getSignal(long timeout) {
		String signal = null;
		while ((signal = _queue.poll()) == null && timeout > 0) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			timeout--;
		}
		return signal;
	}
        
        public static void main(String[] args){
            UdpReceiver haha = new UdpReceiver();
            haha.listen(10012, 1024);
        }

}

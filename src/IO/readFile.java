/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author THEDE
 */
public class readFile {
    public ArrayList<String> FileToArrayString(String file) throws IOException{
        ArrayList<String> data = new ArrayList<String>();
        BufferedReader br = null;
        String sCurrentLine;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(readFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        while ((sCurrentLine = br.readLine()) != null) {
            data.add(sCurrentLine);
	}
        return data;
    }
}
